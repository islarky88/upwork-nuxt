import axios from 'axios'
export const state = () => {
	return {
    alert: 'amsbot',
    count: 2,
    posts: '',
    todos: [
      { id: 1, text: '...', done: true },
      { id: 2, text: '...', done: false }
    ]
	};
};

export const actions = {
	async GET_STARS({ commit }, payload) {
    //	const { data } = await axios.get('http://my-api/stars');
    let msg = '';

    payload.forEach(item => {
      msg += item.title;
    });

		commit('changeAlert', 'post updated');
  },
  async getNewPosts({ commit }, payload) {
    const { data } = await axios.get(payload);

setTimeout(() => {
  commit('changeAlert', 'post updated');

}, 2000);


		commit('updateNewPost', data);
	}
};

export const mutations = {
	changeAlert(state, payload) {
		state.alert = payload;
  },
  updateNewPost(state, payload) {
    state.posts = payload;
  },
  updateState(state, payload) {
    state[payload.prop] = payload.value;
  }
};

// store/todo.js
export const getters =  {
  getTodoById: (state) => (id) => {
    return state.todos.find(todo => todo.id === id)
  },
  doneTodos: state => {
    return state.todos.filter(todo => todo.done)
  }
}

